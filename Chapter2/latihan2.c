/**
 * @author: Utoro A
 * @created: Kam 18 Okt 2018 15:12:04  WIB
 *
 * @repository: https://gitlab.com/_utoro/spt.git
 * @folder : Chapter2
 **/

#include <omp.h>
#include <stdio.h>

#define N 16

int main()
{
	int A[N] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
	int i, sum = 0;
    
    #pragma omp parallel num_threads(4)
    {
    	#pragma omp for
    	for (i = 0; i<N; i++){
    		#pragma omp critical
        	sum += A[i];
        }
	}
    printf("%d\n", sum);
    
    return 0;
}
