/**
 * @author: Utoro A
 * @created: Jum 26 Okt 2018 11:53:56  WIB
 *
 * @repository: https://gitlab.com/_utoro/spt.git
 * @folder : Chapter2
 **/

#include <omp.h>
#include <stdio.h>

#define M 4		// row
#define N 4		// column

int main()
{
	int A[M][N] = {{2, 1, 0, 4},
				   {3, 2, 1, 1},
				   {4, 3, 1, 2},
				   {3, 0, 2, 0}};
	int b[N] = {1, 3, 4, 1};
	int c[M] = {};
	int i, j;

	double time = omp_get_wtime();

	// c = A*b
	for (i = 0; i < M; i++)
		for (j = 0; j < N; j++)
			c[i] += A[i][j] * b[j];


	time = omp_get_wtime() - time;
	
	// print c
	for (i = 0; i < M; i++)
		printf("%d\n", c[i]);

	printf("%lf\n", time);
	return 0;
}
