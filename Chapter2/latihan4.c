/**
 * @author: Utoro A
 * @created: Kam 25 Okt 2018 15:11:04  WIB
 *
 * @repository: https://gitlab.com/_utoro/spt.git
 * @folder : Chapter2
 **/

#include <omp.h>
#include <stdio.h>
#include <limits.h>

#define N 16

int main()
{
	int A[N] = {45,16,68,69,1,90,0,29,88,42,0,72,0,40,24,21};
	int i, maks = INT_MIN;

	#pragma omp parallel num_threads(4)
	{
		#pragma omp for reduction(max:maks)
    	for (i = 0; i<N; i++){
        	if(A[i] > maks)
            	maks = A[i];
    	}
	}
    printf("%d\n", maks);
    
    return 0;
}
