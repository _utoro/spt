/**
 * @author: Utoro A
 * @created: unknown
 *
 * @repository: https://gitlab.com/_utoro/spt.git
 * @folder : Chapter1
 **/

#include <omp.h>
#include <stdio.h>

#define N 1200
#define T 4

int main()
{
	int sum = 0;
    
    #pragma omp parallel num_threads(T)
    {
    	int i;
    	for (i = 0; i < N/T; i++){
    		#pragma omp critical
        	sum++;		// critical section
        }
	}

    printf("%d\n", sum);
    
    return 0;
}
