/**
 * @author: Utoro A
 * @created: unknown
 *
 * @repository: https://gitlab.com/_utoro/spt.git
 * @folder : Chapter1
 **/

#include <omp.h>
#include <stdio.h>

int main(){
	int i, id;

	#pragma omp parallel private(id)
	{
		id = omp_get_thread_num();

		#pragma omp for
		for (int i = 0; i < 16; ++i)
			printf("Thread-%d: index-%d\n", id, i);
	}

	return 0;
}