/**
 * @author: Utoro A
 * @created: unknown
 *
 * @repository: https://gitlab.com/_utoro/spt.git
 * @folder : Chapter1
 **/

#include <omp.h>
#include <stdio.h>

int main(){
	int total = 0;

	#pragma omp parallel reduction(+:total)
	total = 1;

	printf("%d\n", total);

	return 0;
}