/**
 * @author: Utoro A
 * @created: unknown
 *
 * @repository: https://gitlab.com/_utoro/spt.git
 * @folder : Chapter1
 **/

#include <stdio.h>

int main(){
	#pragma omp parallel num_threads(4)
    printf("Hello, World!\n");

    return 0;
}
