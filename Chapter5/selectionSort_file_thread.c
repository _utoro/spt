/**
 * _utoro
 *
 * https://gitlab.com/_utoro/spt.git
 **/

#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <sys/sysinfo.h>

// http://madhugnadig.com/articles/parallel-processing/2017/02/25/parallel-computing-in-c-using-openMP.html
struct Compare { int val; int index; };
#pragma omp declare reduction(maximum : struct Compare : omp_out = omp_in.val > omp_out.val ? omp_in : omp_out)

typedef struct fileData{
    int arrSize;
    int* data;
} fileData;

void swap(int* a, int* b);
void selectionSort(int* A, int n);
fileData readFileToArr(char const *fileName);
void writeFile(char const *fileName, int* data, int size);


int main(int argc, char const *argv[]){

    if(argv[1] == NULL || argv[2] == NULL){
        printf("Usage: %s inputFile outputFile [thread_amount]\n", argv[0]);
        return -1;
    }
    if (argv[3] == NULL){
        omp_set_num_threads(get_nprocs_conf());
    }else if (atoi(argv[3]) > get_nprocs_conf()){
        omp_set_num_threads(atoi(argv[3]));
        printf("%s is too much, set to %d instead for better result\n", argv[3], get_nprocs_conf());
    }else{
        omp_set_num_threads(atoi(argv[3]));
    }

    printf("Number of threads: %d\n", omp_get_max_threads());

    fileData inputData = readFileToArr(argv[1]);

    double time = omp_get_wtime();
    selectionSort(inputData.data, inputData.arrSize);
    time = omp_get_wtime() - time;

    writeFile(argv[2], inputData.data, inputData.arrSize);

    printf("Time taken for sort : %lf\n", time);
    printf("The amount of data  : %d\n", inputData.arrSize);

    return 0;
}

void selectionSort(int* A, int n){
    for(int startpos =n-1; startpos > 0; --startpos){
        struct Compare max;
        max.val = A[startpos];
        max.index = startpos;

        #pragma omp parallel for reduction(maximum:max)
        for(int i=startpos -1; i>= 0; --i){
            if(A[i] > max.val){
                max.val = A[i];
                max.index = i;
            }
        }
        swap(&A[startpos], &A[max.index]);
    }
}

void swap(int* a, int* b){
    int temp = *a;
    *a = *b;
    *b = temp;
}

fileData readFileToArr(char const *fileName){
    FILE *fli;
    int count = 0;
    char buf[255];
    int i;
    fileData result;

    // open the file
    fli = fopen(fileName, "r");
    if (fli == NULL)
    {
        printf("ERROR File not found\n");
        exit(1);
    }
    // count the number of lines
    while (!feof(fli)){
        fgets(buf, 255, fli);
        count++;
    }
    count--;
    result.arrSize = count;
    // go back to beginning of file
    fseek(fli, 0, SEEK_SET);
    // load in the data
    result.data = (int*)malloc(sizeof(int) * result.arrSize);
    for (i = 0; i < count; i++)
        fscanf(fli, "%d\n", &result.data[i]);
    // fclose
    fclose(fli);

    return result;
}

void writeFile(char const *fileName, int* data, int size){
    FILE *flo;

    flo = fopen(fileName, "w");
    if(flo == NULL){
        printf("ERROR creating file!\n");
        exit(1);
    }
    // create result file
    for (int i = 0; i < size; ++i)
        fprintf(flo, "%d\n", data[i]);
    fclose(flo);
    printf("File is created.\n");
}
