/**
 * _utoro
 *
 * https://gitlab.com/_utoro/spt.git
 **/

#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

typedef struct fileData{
    int arrSize;
    int* data;
} fileData;

void swap(int* a, int* b);
void selectionSort(int* A, int n);
fileData readFileToArr(char const *fileName);
void writeFile(char const *fileName, int* data, int size);

int main(int argc, char const *argv[]){
    fileData inputData = readFileToArr(argv[1]);

    double time = omp_get_wtime();
    selectionSort(inputData.data, inputData.arrSize);
    time = omp_get_wtime() - time;

    writeFile(argv[2], inputData.data, inputData.arrSize);
    printf("Time taken for sort : %lf\n", time);
    printf("The amount of data  : %d\n", inputData.arrSize);

    return 0;
}

void selectionSort(int* A, int n){
    for(int startpos =0; startpos < n; startpos++){
        int maxpos = startpos;
        for(int i=startpos +1; i< n; ++i){
            if(A[i] < A[maxpos]){
                maxpos = i;
            }
        }
        swap(&A[startpos], &A[maxpos]);
    }
}

void swap(int* a, int* b){
    int temp = *a;
    *a = *b;
    *b = temp;
}

fileData readFileToArr(char const *fileName){
    FILE *fli;
    int count = 0;
    char buf[255];
    int i;
    fileData result;

    // open the file
    fli = fopen(fileName, "r");
    if (fli == NULL)
    {
        printf("ERROR File not found\n");
        exit(1);
    }
    // count the number of lines
    while (!feof(fli)){
        fgets(buf, 255, fli);
        count++;
    }
    count--;
    result.arrSize = count;
    // go back to beginning of file
    fseek(fli, 0, SEEK_SET);
    // load in the data
    result.data = (int*)malloc(sizeof(int) * result.arrSize);
    for (i = 0; i < count; i++)
        fscanf(fli, "%d\n", &result.data[i]);
    // fclose
    fclose(fli);

    return result;
}

void writeFile(char const *fileName, int* data, int size){
    FILE *flo;

    flo = fopen(fileName, "w");
    if(flo == NULL){
        printf("ERROR creating file!\n");
        exit(1);
    }
    // create result file
    for (int i = 0; i < size; ++i)
        fprintf(flo, "%d\n", data[i]);
    fclose(flo);
    printf("File is created.\n");
}
