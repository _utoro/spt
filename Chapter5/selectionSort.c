/**
 * _utoro
 *
 * https://gitlab.com/_utoro/spt.git
 **/

#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

void swap(int* a, int* b);
void selectionSort(int* A, int n);

int main(){
    int number, iter =0;

    printf("Enter number of element: ");
    scanf("%d", &number);

    int Arr[number];

    printf("Enter %d integer:\n", number);

    for (iter; iter < number; iter++)
        scanf("%d", &Arr[iter]);

    double time = omp_get_wtime();
    selectionSort(Arr, number);
    time = omp_get_wtime() - time;

    printf("\nAfter sorting: \n");
    for (iter = 0; iter < number; iter++)
        printf("%d ", Arr[iter]);

    printf("\n\nTime taken for sort: %lf\n", time);

    return 0;
}

void selectionSort(int* A, int n){
    for(int startpos =0; startpos < n; startpos++){
        int maxpos = startpos;
        for(int i=startpos +1; i< n; ++i){
            if(A[i] < A[maxpos]){
                maxpos = i;
            }
        }
        swap(&A[startpos], &A[maxpos]);
    }
}

void swap(int* a, int* b){
    int temp = *a;
    *a = *b;
    *b = temp;
}
