#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
    FILE *fp;
    int i = 0;
    int num, max_num, j;

    printf("Enter the number of recors: " );
    scanf("%d", &max_num);

    fp = fopen(argv[1], "w");
    if(fp == NULL){
        printf("ERROR creating file!");
        exit(1);
    }

    // random algorithm in here
    for (i = 0; i < max_num; ++i)
    {
        num = rand() % max_num + 1;
        fprintf(fp, "%d\n", num);
    }

    fclose(fp);
    printf("\nFile is created.\n");

    return 0;
}
