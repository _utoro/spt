/**
 * _utoro
 *
 * https://gitlab.com/_utoro/spt.git
 **/

#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

// http://madhugnadig.com/articles/parallel-processing/2017/02/25/parallel-computing-in-c-using-openMP.html
struct Compare { int val; int index; };
#pragma omp declare reduction(maximum : struct Compare : omp_out = omp_in.val > omp_out.val ? omp_in : omp_out)

void swap(int* a, int* b);
void selectionSort(int* A, int n);

int main(){
    int number, iter =0;

    printf("Enter number of element: ");
    scanf("%d", &number);

    int Arr[number];

    printf("Enter %d integer:\n", number);

    for (iter; iter < number; iter++)
        scanf("%d", &Arr[iter]);

    double time = omp_get_wtime();
    selectionSort(Arr, number);
    time = omp_get_wtime() - time;

    printf("\nAfter sorting: \n");
    for (iter = 0; iter < number; iter++)
        printf("%d ", Arr[iter]);

    printf("\n\nTime taken for sort: %lf\n", time);

    return 0;
}

void selectionSort(int* A, int n){
    for(int startpos =n-1; startpos > 0; --startpos){
        struct Compare max;
        max.val = A[startpos];
        max.index = startpos;

        #pragma omp parallel for reduction(maximum:max)
        for(int i=startpos -1; i>= 0; --i){
            if(A[i] > max.val){
                max.val = A[i];
                max.index = i;
            }
        }
        swap(&A[startpos], &A[max.index]);
    }
}

void swap(int* a, int* b){
    int temp = *a;
    *a = *b;
    *b = temp;
}
