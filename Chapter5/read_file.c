#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
    FILE *fid;
    int nr = 0;
    char buf[255];

    // open the file
    fid = fopen(argv[1], "r");

    // count the number of lines
    while (!feof(fid)) {
        fgets(buf, 255, fid);
        nr++;
    }
    nr--;

    // go back to beginning of file
    fseek(fid, 0, SEEK_SET);

    // load in the data
    int c1[nr];
    int i;
    for (i = 0; i < nr; i++) {
        fscanf(fid, "%d\n", &c1[i]);
    }

    // fclose
    fclose(fid);

    // print the data to the screen
    for (i = 0; i < nr; i++) {
        printf("%d\n", c1[i]);
    }

    return 0;
}