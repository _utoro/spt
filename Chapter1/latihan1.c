/**
 * @author: Utoro A
 * @created: Kam 11 Okt 2018 15:12:04  WIB
 *
 * @repository: https://gitlab.com/_utoro/spt.git
 * @folder : Chapter1
 **/

#include <omp.h>
#include <stdio.h>

int main()
{
    #pragma omp parallel num_threads(4)
    {
        int id = omp_get_thread_num();
        printf("thread id = %d\n", id);
    }
    return 0;
}
