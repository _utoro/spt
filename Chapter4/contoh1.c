#include <pthread.h>
#include <stdio.h>

// Kam 13 Des 2018 15:20:53  WIB

void *hello(void *arg) {
    printf("hello\n");
    pthread_exit(NULL);
}

int main(){
    pthread_t thread;
    pthread_create(&thread, NULL, hello, NULL);
    pthread_join(thread, NULL);
    return 0;
}