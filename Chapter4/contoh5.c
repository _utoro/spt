#include <pthread.h>
#include <stdio.h>
// Kam 13 Des 2018 15:57:12  WIB
void* hello(void* arg){
    printf("hello from thread %s\n", (char*)arg);
    pthread_exit(NULL);
}
int main() {
    pthread_t thread1;
    pthread_t thread2;
    pthread_create(&thread1, NULL, hello, "0");
    pthread_create(&thread2, NULL, hello, "1");
    pthread_join(thread1, NULL); pthread_join(thread2, NULL);

    return 0;
}