#include <pthread.h>
#include <stdio.h>
// Kam 13 Des 2018 16:11:19  WIB
#define N 4
void* hello(void* arg){
    printf("hello from thread %s\n", (char*)arg);
    pthread_exit(NULL);
}
int main() {
    pthread_t thread[N];
    char *id[N] = {"0", "1", "2", "3"};
    int i;

    for (i = 0; i < N; i++)
        pthread_create(&thread[i], NULL, hello, id[i]);

    for (i = 0; i < N; i++)
        pthread_join(thread[i], NULL);

    return 0;
}