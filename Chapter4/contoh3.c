// Kam 13 Des 2018 15:53:13  WIB
#include <pthread.h>
#include <stdio.h>

#define N 4

void *hello(void *arg) {
    printf("hello\n");
    pthread_exit(NULL);
}

int main() {
    pthread_t thread[N];
    int i;
    for (i = 0; i < N; i++)
        pthread_create(&thread[i], NULL, hello, NULL);

    for (i = 0; i < N; i++)
        pthread_join(thread[i], NULL);
    return 0;
}