#include <pthread.h>
#include <stdio.h>
// Kam 13 Des 2018 15:55:52  WIB

void* hello(void* arg){
    printf("hello from thread %s\n", (char*)arg);
    pthread_exit(NULL);
}

int main() {
    pthread_t thread;
    pthread_create(&thread, NULL, hello, "0");
    pthread_join(thread, NULL); return 0;
}