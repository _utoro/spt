/**
 * @author: Utoro A
 *
 * REQUEST ACCESS NEEDED
 * @repository: https://gitlab.com/_utoro/spt.git
 * @folder : 2
 **/

#include <stdio.h>

int main(){
    float r = 0;
    const float PHI = 3.14159;
    printf("Enter cicrle radius: ");
    scanf("%f", &r);

    printf("\nRadius: %g\n", r);
    printf("Diameter: %g\n", 2*r);
    printf("Area: %g\n", PHI*r*r);
    printf("Circumference: %g\n", 2*PHI*r);

    return 0;
}
